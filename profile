#!/usr/bin/env bash
is_git_repo() {
  [ -d "$1/.git" ]  
}

bash_update_prompt() {
  bash_status="?$?" 
  is_git_repo $(pwd) && bash_status+=" $(__git_ps1 %s)"
}

bash_init_prompt() {
  GIT_PS1_SHOWDIRTYSTATE=true
  GIT_PS1_SHOWUNTRACKEDFILES=true
  GIT_PS1_SHOWSTASHSTATE=true
  GIT_PS1_SHOWUPSTREAM=auto
  PS1="\[\033[01;32m\]\u@\h\[\033[00m\] (\$bash_status) \[\033[01;34m\]\w\[\033[00m\]\n\$ "
}

__debug() {
  [ $NCT_DEBUG ] && $*
}

__guard() {
  [ ! $__guard__ ] && declare -A __guard__
  [ ${__guard__[$1]} ] && { return 1; } || {
    __guard__[$1]=1 
    return 0
  }
}

__nct_source() {
  __debug echo "sourcing $1"
  [ -s $NCT_HOME/$1 ] && source $NCT_HOME/$1 || __debug echo "failed to source $1" >&2
}

__nct_reload_all() {
  unset __guard__
  __nct_source profile
}

__quiet() {
  $* 2>&1 >/dev/null
  return $?
}

__noerr() {
  $* 2>/dev/null
  return $?
}

__osx_clip() {
  __quiet which pbpaste || return
  __noerr pbpaste
}

__linux_clip() {
  __quiet which xclip || return
  __noerr xclip -sel clipboard -o 2>&1
}

case $(uname) in
  Darwin) clip() { __osx_clip; } ;;
  *) clip() { __linux_clip; } ;;
esac

PROMPT_COMMAND=bash_update_prompt

bash_init_prompt
unset bash_init_prompt

__nct_source completion
__nct_source prj
